module StackCalculator

type Stack = StackContents of float list

let push x (StackContents contents) =
    StackContents (x::contents)

let ONE = push 1.0
let TWO = push 2.0
let THREE = push 3.0
let FOUR = push 4.0
let FIVE = push 5.0

let EMPTY = StackContents []

let pop (StackContents contents) =
    match contents with
    | top::rest ->
        let newStack = StackContents rest
        (top, newStack)
    | [] ->
        failwith "Stack underflow"

let binaryOperator operator stack =
    let x, stack' = pop stack
    let y, stack'' = pop stack'
    let result = operator x y
    push result stack''

let unaryOperator operator stack =
    let x, stack' = pop stack
    let result = operator x
    push result stack'

let ADD = binaryOperator (+)

let MUL = binaryOperator (*)

let SUB = binaryOperator (-)

let DIV = binaryOperator (/)

let NEG = unaryOperator (~-)

let SQUARE = unaryOperator (fun x -> x * x)

let SHOW stack =
    let top,_ = pop stack
    printfn "Top = %f" top
    stack

let DUP stack =
    let x, _ = pop stack
    push x stack

let SWAP stack =
    let x, stack' = pop stack
    let y, stack'' = pop stack'
    push y (push x stack'')

[<EntryPoint>]
let main argv =
    printfn "%A" argv
    0 // return an integer exit code
